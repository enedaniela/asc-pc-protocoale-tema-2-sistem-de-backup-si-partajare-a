#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdio>
#include <cstring>
#include <map>
#include <sstream>
#include <iterator>
#include <vector>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>
#include <unistd.h>
#include <fstream>

using namespace std;

class File {
public:
	string owner;
    string name;
    long dim;
    bool privacy;
    bool isBusy;
  public:
     File (string,string,long,bool,bool);
};

File::File (string utiliz, string nume, long dim_fisier, bool partajat, bool unav) {
	owner = utiliz;
	name = nume;
	dim = dim_fisier;
	privacy = partajat;
	isBusy = unav;
}

#define MAX_CLIENTS	10
#define BUFLEN 4096

void error(char *msg)
{
    perror(msg);
    exit(1);
}
std::map<std::string, std::string> credentiale;
std::vector<File> sharedfiles;
vector<File> allfiles;
std::map<int, std::string> errors;
string users = "";

//functie ce intoarce dimensiunea unui fisier
long GetFileSize(std::string filename)
{
    struct stat stat_buf;
    int rc = stat(filename.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

//functii auxiliare pentru afisaj
void show_credentials(){

	std::map<string,string>::iterator it = credentiale.begin();

	std::cout << "Tabela de utilizatori contine:\n";
	for (it=credentiale.begin(); it!=credentiale.end(); ++it)
		std::cout << it->first << " => " << it->second << '\n';
}

void show_files(vector<File> vec){

	std::cout << "Tabela de fisiere contine:\n";
	for (int i = 0; i < vec.size(); i++)
		cout<< vec.at(i).owner<<" "<<vec.at(i).name<<"\n";
}

//functie care parseaza dupa ' '
vector<string> parse(string str){

	std::istringstream buf(str);
    std::istream_iterator<std::string> beg(buf), end;

    std::vector<std::string> tokens(beg, end); // done!
    return tokens;
}

//functie ce populeaza map-ul de utilizatori
void populate_users(const std::string str){

    std::vector<std::string> tokens;
    tokens = parse(str);
    std::pair<std::map<string,string>::iterator,bool> ret;
    ret = credentiale.insert ( std::pair<string,string>(tokens.at(0),tokens.at(1)) );
    users.append(tokens.at(0));
    users.append("\n");
    if (ret.second==false) {
    std::cout << "The username is already used\n";
    
  }
}

//functie ce populeaza vectorul de obiecte de tip File din
//fisierul shared_files
void loadSharedFiles(char* shared_files){
	char line[256];
	int i, total_files;
	std::string segment;
	std::vector<std::string> seglist;
	long fileDim = 0;

    ifstream myfile;
  	myfile.open(shared_files);
    myfile >> total_files;
	myfile.getline(line,256);
	for( i = 0; i < total_files; i++)
    {
    	string path = "";
    	myfile.getline(line,256);
    	std::stringstream test(line);
      	while(std::getline(test, segment, ':'))
		{
		   seglist.push_back(segment);
		}
		path.append(seglist.at(2*i));
		path.append(1u,'/');
		path.append(seglist.at(2*i+1));
		fileDim = GetFileSize(path);

		File f (seglist.at(2*i),seglist.at(2*i+1),fileDim,true, false);
		sharedfiles.push_back(f);
		
    }
    myfile.close();
}

//functie ce populeaza map-ul de erori
void populate_errors(){

	errors.insert(pair<int, string>(-1,"-1 Clientul nu e autentificat"));
	errors.insert(pair<int, string>(-2,"-2 Sesiune deja deschisa"));
	errors.insert(pair<int, string>(-3,"-3 User/parola gresita"));
	errors.insert(pair<int, string>(-4,"-4 Fisier inexistent"));
	errors.insert(pair<int, string>(-5,"-5 Descarcare interzisa"));
	errors.insert(pair<int, string>(-6,"-6 Fisier deja partajat"));
	errors.insert(pair<int, string>(-7,"-7 Fisier deja privat"));
	errors.insert(pair<int, string>(-8,"-8 Brute-force detectat"));
	errors.insert(pair<int, string>(-9,"-9 Fiser existent pe server"));
	errors.insert(pair<int, string>(-10,"-10 Fisier in transfer"));
	errors.insert(pair<int, string>(-11,"-11 Utilizator inexistent"));
}

//fuctie ce returneaza un string cu toti utilizatori
//construit la popularea map-ului de utilizatori
string getuserlist(){
	return users;
}

//functie ce parcurge vectorul de fisiere
//si construieste un string cu detalii despre acestea
string getfilelist(string owner){
	string str;
	map<string,string>::iterator it;

	it = credentiale.find(owner);
	if(it == credentiale.end())
		str = errors[-11];
	else{
		for(int i = 0; i < allfiles.size(); i++){
			if(allfiles.at(i).owner == owner){
				str.append(allfiles.at(i).name);
				str.append("\t");
				std::ostringstream ss;
				ss << allfiles.at(i).dim;
				str.append(ss.str());
				str.append(" bytes");
				str.append("\t");
				if(allfiles.at(i).privacy == true)
					str.append("SHARED");
				else
					str.append("PRIVAT");
				str.append("\n");
			}
		}
	}

	return str;
}

//functie ce cauta in map-ul de utilizatori
//si valideaza userul si parola
string login(string user, string password){
	map<string,string>::iterator it;
	it = credentiale.find(user);
	if(it != credentiale.end())
		if(credentiale[user] == password)
			return user;
	return errors[-3];
}

//functie ce cauta un fisier in vectorul de fisiere
//si ii schimba campul "privacy"
string shareOrUnshare(string user, string file, bool how){
	for(int i = 0; i < allfiles.size(); i++){
		if(allfiles.at(i).owner == user && allfiles.at(i).name == file)
			if(how == true){//vreau sa schimb in SHARED
				if(allfiles.at(i).privacy == false){//fac schimbarea
					allfiles.at(i).privacy = true;
					return "200 Fisierul " + file + " a fost partajat.";
				}
				else
					return errors[-6];//fisierul e deja SHARED
			}
			else{//vreau sa schimb in PRIVAT
				if(allfiles.at(i).privacy == true){
					allfiles.at(i).privacy = false;
					return "200 Fisierul a fost setat ca PRIVAT.";
				}
				else
					return errors[-7];//fisierul e deja PRIVAT
			}		
	}
	return errors[-4];
}	

//functie ce cauta un fisier dat ca parametru pe disk
string checkIfFile(string user, string file, string what){
	bool ok = false;
	DIR* Dir;
	struct dirent *DirEntry;

	user.append(1u,'/');
	const char *cstr = user.c_str();
	Dir = opendir(cstr);

	if (Dir == NULL) {
            cout << "Cannot open directory " << user <<"\n";          
        }
    else{
		while(DirEntry=readdir(Dir))
		{
		   if(DirEntry->d_name == file)
		   		ok = true;
		}
	}
	closedir(Dir);
	if(!ok && what == "check"){
		cout<<"The file "<<file<<" does not exist on disk in "<<user<<" folder. Please check shared_files!"<<"\n";
	}
	if(ok && what == "upload")
		return errors[-9];
	return file;
	
}

//functie ce parcurge toate fisierele de pe disk
//si populeaza vectorul de fisiere
void loadAllFiles(){
	string currentPath;
	DIR* Dir;
	struct dirent *DirEntry;
	std::map<string,string>::iterator it = credentiale.begin();	

	for (it=credentiale.begin(); it!=credentiale.end(); ++it){
		currentPath  = it->first; 
		currentPath.append(1u,'/');
		const char *cstr = currentPath.c_str();
		Dir = opendir(cstr);//am deschis
		if(Dir != NULL){
			while(DirEntry = readdir(Dir)){//pentru fiecare fisier 
				if(strcmp(DirEntry->d_name, ".") != 0 && strcmp(DirEntry->d_name, "..") != 0){

					long fileDim = GetFileSize(currentPath+DirEntry->d_name);
					File f (it->first,DirEntry->d_name,fileDim,false, false);
					allfiles.push_back(f);

				}
			}
		}
		closedir(Dir);
	}
}

//functie ce parcurge vectorul cu toate fisierele si pe cel
//cu fisiere shared si schimba, in primul vector, in mod corespunzator
//campul "privacy"
void mixFiles(){
	for(int i = 0; i < allfiles.size(); i++)
		for(int j = 0; j < sharedfiles.size(); j++){
			if(sharedfiles.at(j).owner == allfiles.at(i).owner && sharedfiles.at(j).name == allfiles.at(i).name)
				allfiles.at(i).privacy = true;
		}
}

//functie ce verifica corectitudinea informatiilor din shared_files
void ckeckConfigFile(){

	std::map<string,string>::iterator it;
	for(int i = 0; i < sharedfiles.size(); i++){
		it = credentiale.begin();
		it = credentiale.find(sharedfiles.at(i).owner);
		if (it == credentiale.end())
		    cout<<"User "<<sharedfiles.at(i).owner<<" does not exist in system. Please check shared_files!\n";
		else{
			checkIfFile(sharedfiles.at(i).owner, sharedfiles.at(i).name, "check");
		}

	 }
}

//functie care trimite un mesaj catre client
void send_message(string str, int i){
	int n;
	const char * c = str.c_str();
	n = send(i,c,strlen(c), 0);
	if (n < 0) {
        char msg[] = "ERROR writing to socket";
    	error(msg);
    }
}

//functie ce construieste numele unui fisier
//in cazul in care contine spatii
string appendStrings(vector<string> command){
	string str = "";
	for(int i = 1; i < command.size() - 1; i++){
		str.append(command.at(i));
		str.append(1u,' ');
	}
	str.pop_back();
	return str;
}

//functie ce construieste numele unui fisier
//in cazul in care contine spatii
string appendStringsDownload(vector<string> command){
	string str = "";
	for(int i = 2; i < command.size() - 1; i++){
		str.append(command.at(i));
		str.append(1u,' ');
	}
	str.pop_back();
	return str;
}

//verificare conditii pentru download
string downloadFile(string file, string folder){
	for(int i = 0; i < allfiles.size(); i++){
		if(allfiles.at(i).name == file && allfiles.at(i).owner == folder){
			if(allfiles.at(i).privacy == true){
				return "Dowloading..." + file;//TODO: real download
			}
			else
				return errors[-5];
		}
	}
	return errors[-4];
}

//functie ce sterge un fisier
string deleteFile(string owner, string file){

	for(int i = 0; i < allfiles.size(); i++)
		if(allfiles.at(i).owner == owner && allfiles.at(i).name == file)
			if(allfiles.at(i).isBusy == true)
				return "-10 File is busy";

	string str;
	str.append(owner);
	str.append(1u,'/');
	str.append(file);
	const char *cstr = str.c_str();
	if(unlink(cstr) != 0)
		return "Error deleting the file\n";
	else{
		for(int i = 0; i < allfiles.size(); i++){
			if(allfiles.at(i).owner == owner && allfiles.at(i).name == file)
				allfiles.erase(allfiles.begin() + i);
		}
		return "200 Fisier sters\n";
	}
}

int main(int argc, char *argv[])
{
//TODO: Pus numele fisierelor de configurare ca parametrii in linia de comanda
//TODO: Pune toate mesajele intr-o singura limba
	char line[256];
	int total_users, total_files, i, bruteForce = 0;
	ifstream myfile;
  	myfile.open (argv[2]);
  	
	myfile >> total_users;
	myfile.getline(line,256);
    for( i = 0; i < total_users; i++)
    {
    	myfile.getline(line,256);
      	populate_users(line);
    }
    myfile.close();
    populate_errors();
    loadSharedFiles(argv[3]);
    loadAllFiles();
    mixFiles();
    ckeckConfigFile();
    
     char bufferSend[BUFLEN];
     int sockfd, newsockfd, portno;
     int socktosend;
     socklen_t clilen;
     char buffer[BUFLEN];
     char fullComand[BUFLEN];
     struct sockaddr_in serv_addr, cli_addr;
     int n, j;
     string str;
     fd_set read_fds;	//multimea de citire folosita in select()
     fd_set tmp_fds;	//multime folosita temporar 
     fd_set tmp_fdss;	//alta multime folosita temporar
     int fdmax;		//valoare maxima file descriptor din multimea read_fds

     if (argc < 2) {
         fprintf(stderr,"Usage : %s port\n", argv[0]);
         exit(1);
     }

     //golim multimea de descriptori de citire (read_fds) si multimea tmp_fds 
     FD_ZERO(&read_fds);
     FD_ZERO(&tmp_fds);
     
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) {
     	char msg[] = "ERROR opening socket";
        error(msg);
    }     
     portno = atoi(argv[1]);

     memset((char *) &serv_addr, 0, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;	// foloseste adresa IP a masinii
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr)) < 0) {
     	char msg[] = "ERROR on binding";
        error(msg);
    }
     
     listen(sockfd, MAX_CLIENTS);
	 FD_SET(0,&read_fds);
     //adaugam noul file descriptor (socketul pe care se asculta conexiuni) in multimea read_fds
     FD_SET(sockfd, &read_fds);
     

     fdmax = sockfd;
     // main loop
	while (1) {
		tmp_fds = read_fds;
		if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) {
			char msg[] = "ERROR in select";
			error(msg);
		}
		
		for(i = 0; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_fds)) {
				//daca exista activitate de la stdin
				if(i == 0){
					string sen = "closing";
		        	fgets(bufferSend, BUFLEN-1, stdin);
		        	string servcmd(bufferSend);
		        	//daca comanda este quit
		        	if(servcmd == "quit\n"){
						for(int j = 1; j <= fdmax; j++){
							if(FD_ISSET(j, &read_fds)){
					        	send_message(sen,j);
					        	close(j);
								FD_CLR(j, &read_fds);
							}
				            }
			            close(0);
			            exit(0);
			        }
				}
				else if (i == sockfd) {
					// a venit ceva pe socketul inactiv(cel cu listen) = o noua conexiune
					// actiunea serverului: accept()
					clilen = sizeof(cli_addr);
					if ((newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen)) == -1) {
						char msg[] = "ERROR in accept";
						error(msg);
					} 
					else {
						//adaug noul socket intors de accept() la multimea descriptorilor de citire
						FD_SET(newsockfd, &read_fds);
						if (newsockfd > fdmax) { 
							fdmax = newsockfd;
						}
					}
					printf("Noua conexiune de la %s, port %d, socket_client %d\n ", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port), newsockfd);
				}
				else {
					// am primit date pe unul din socketii cu care vorbesc cu clientii
					//actiunea serverului: recv()
					memset(buffer, 0, BUFLEN);
					if ((n = recv(i, buffer, sizeof(buffer), 0)) <= 0) {
						if (n == 0) {
							//conexiunea s-a inchis
							printf("selectserver: socket %d hung up\n", i);
						} else {
							char msg[] = "ERROR in recv";
							error(msg);
						}
						close(i); 
						FD_CLR(i, &read_fds); // scoatem din multimea de citire socketul pe care 
					} 
					
					else {
						printf ("Am primit de la clientul de pe socketul %d, mesajul: %s\n", i, buffer);
						string com0 = "login";
						string com1 = "getuserlist\n";
						string com2 = "getfilelist";
						string com3 = "quit\n";
						string com4 = "upload";
						string com5 = "share";
						string com6 = "unshare";
						string com8 = "download";
						string com9 = "delete";
						string strWord(buffer);
						if (strWord.find(com0) != std::string::npos){
							vector<string> cmd = parse(strWord);
							if(cmd.size() < 3){
								str = "Comanda invalida: " + com0 +"\n";
							}
							else{
								str = login(cmd.at(1),cmd.at(2));
								if(str == errors[-3])
									bruteForce++;
								else{
									bruteForce = 0;
								}
								if(bruteForce == 3)
									str = errors[-8];
							}

						}else if (strWord.find(com2) != std::string::npos){
							vector<string> cmd = parse(strWord);
							if(cmd.size() < 2)
								str = "Comanda invalida: " + com2 + "\n";
							else
								str = getfilelist(parse(strWord).at(1));

						}
						else if(com3 == strWord){
							str = "quit";	

						}
						else if(com1 == strWord){
							str = getuserlist();

						}
						else if(strWord.find(com4) != std::string::npos){
							vector<string> cmd = parse(strWord);
							if(cmd.size() < 3)
								str = "Invalid command: " + com4;
							else
								str = checkIfFile(cmd.back(),appendStrings(cmd),"upload");

						}else if(parse(strWord).at(0) == com5){
							vector<string> cmd = parse(strWord);
							if(cmd.size() < 3)
								str = "Invalid command: " + com5;
							else
								str = shareOrUnshare(cmd.back(),appendStrings(cmd),true);
						}
						else if(parse(strWord).at(0) == com6){
							vector<string> cmd = parse(strWord);
							if(cmd.size() < 3)
								str = "Invalid command: " + com6;
							else
								str = shareOrUnshare(cmd.back(),appendStrings(cmd),false);
						}
						else if(strWord.find(com8) != std::string::npos){
							vector<string> cmd = parse(strWord);
							if(cmd.size() < 3)
								str = "Invalid command: " + com6;
							else{
								string fromWhere;
								if(cmd.at(1) == "@")
									fromWhere = cmd.back();
								else
									fromWhere = cmd.at(1);
								str = downloadFile(appendStringsDownload(cmd), fromWhere);
							}
						}
						else if(strWord.find(com9) != std::string::npos){
							vector<string> cmd = parse(strWord);
							if(cmd.size() < 3)
								str = "Invalid command: " + com6;
							else
								str = deleteFile(cmd.back(),appendStrings(cmd));
						}
						else
							str = strWord;

						send_message(str, i);
					}
				} 
			}
		}
     }

     close(sockfd);
   
     return 0; 
}