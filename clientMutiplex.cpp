#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <arpa/inet.h>
#include <unistd.h>
#include <map>
#include <string>
#include <vector>
#include <iterator>
#include <sstream>
#include <iostream>
#include <dirent.h>
#include <unistd.h>
#include <fstream>

using namespace std;
#define BUFLEN 4096

std::map<int, std::string> errors;
string currentUser = "";
string guestUser = "";

void error(char *msg)
{
    perror(msg);
    exit(0);
}
//functie care parseaza dupa ' '
vector<string> parse(string str){
    std::istringstream buf(str);
    std::istream_iterator<std::string> beg(buf), end;

    std::vector<std::string> tokens(beg, end); // done!
    return tokens;
}

//construiesc un map cu textele erorilor
void populate_errors(){

    errors.insert(pair<int, string>(-1,"-1 Clientul nu e autentificat"));
    errors.insert(pair<int, string>(-2,"-2 Sesiune deja deschisa"));
    errors.insert(pair<int, string>(-3,"-3 User/parola gresita"));
    errors.insert(pair<int, string>(-4,"-4 Fisier inexistent"));
    errors.insert(pair<int, string>(-5,"-5 Descarcare interzisa"));
    errors.insert(pair<int, string>(-6,"-6 Fisier deja partajat"));
    errors.insert(pair<int, string>(-7,"-7 Fisier deja privat"));
    errors.insert(pair<int, string>(-8,"-8 Brute-force detectat"));
    errors.insert(pair<int, string>(-9,"-9 Fiser existent pe server"));
    errors.insert(pair<int, string>(-10,"-10 Fisier in transfer"));
    errors.insert(pair<int, string>(-11,"-11 Utilizator inexistent"));
}

//functie folosita pentru a construi numele unui fisier ce contine spatii
string appendStrings(vector<string> command){
    string str = "";
    for(int i = 1; i < command.size(); i++){
        str.append(command.at(i));
        str.append(1u,' ');
    }
    str.pop_back();
    return str;
}

//returneaza PID
string getPID(){
    pid_t pid;
    pid = getpid();
    stringstream ss;
    ss << pid;
    string p = ss.str();
    return p;
}

//creaza fisierul de log
string createLogFile(){
    string p = getPID();
    string fileName = "client-<"+p+">.log";
    std::ofstream outfile (fileName);
    outfile.close();
    return fileName;
}

//afisarea promptului
void showPrompt(fstream& fs){
    if(currentUser == guestUser){
        cout << "$ ";
        fs << "$ ";
        fflush(0);
    }
    else{
        cout << currentUser<<"> ";
        fs << currentUser + "> ";
        fflush(0);
    }
}

int main(int argc, char *argv[])
{
    populate_errors();
    string logFileName = createLogFile();
    int sockfd, n, no = 0;
    bool ok = false, commanOK = true;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    vector<string> tokens;
    std::fstream fs;
    fs.open (logFileName, std::fstream::in | std::fstream::out | std::fstream::app);
    int length = 4095;
    char * buffer = new char [length];

    fd_set read_fds;    //multimea de citire folosita in select()
    fd_set tmp_fds;    //multime folosita temporar 
    int fdmax;     //valoare maxima file descriptor din multimea read_fds

    //golim multimea de descriptori de citire (read_fds) si multimea tmp_fds 
    FD_ZERO(&read_fds);
    FD_ZERO(&tmp_fds);

    char bufferSend[BUFLEN];
    char bufferRecv[BUFLEN];
    char currentCommand[BUFLEN];

    if (argc < 3) {
       fprintf(stderr,"Usage %s server_address server_port\n", argv[0]);
       exit(0);
    }  
    
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        char msg[] = "ERROR opening socket";
        error(msg);
    }
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2]));
    inet_aton(argv[1], &serv_addr.sin_addr);
       
    if (connect(sockfd,(struct sockaddr*) &serv_addr,sizeof(serv_addr)) < 0) {
        char msg[] = "ERROR connecting";
        error(msg);    
    }
    FD_SET(sockfd, &read_fds);
    FD_SET(0,&read_fds);
    fdmax = sockfd;

    showPrompt(fs);

    while(1){
        
        commanOK = true;
        ok = false;
        tmp_fds = read_fds; 
        if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) {
            char msg[] = "ERROR in select";
            error(msg);
        }

        if(FD_ISSET(0, &tmp_fds)){
      		//citesc de la tastatura
        	memset(bufferSend, 0 , BUFLEN);
        	fgets(bufferSend, BUFLEN-1, stdin);
            fs << bufferSend;
            if(strcmp(bufferSend,"\n")){
                
                string command(bufferSend);
                tokens = parse(command);

                if(tokens.at(0) == "login" && currentUser != guestUser){
                   cout << errors[-2] <<"\n";
                   fs << errors[-2] <<"\n";
                   commanOK = false;                   
                }
                else if(tokens.at(0) == "login" && parse(command).size() < 3){
                    cout<<"Invalid command: login\n";
                    fs << "Invalid command: login\n";
                    commanOK = false;
                }
                else if(tokens.at(0) == "logout" && currentUser == guestUser){
                    cout << errors[-1] <<"\n";
                    fs << errors[-1] <<"\n";
                    commanOK = false;
                }
                else if(tokens.at(0) == "logout" && currentUser != guestUser){
                    currentUser = guestUser;
                    commanOK = true;
                }
                else if((tokens.at(0) == "getuserlist" || 
                         tokens.at(0) == "getfilelist") && currentUser == guestUser){
                    cout << "Invalid command: You must login first\n";
                    fs << "Invalid command: You must login first\n";
                    commanOK = false;                  
                }
                else if(tokens.at(0) == "upload"){
                    ok = false;
                    if(tokens.size() < 2 || currentUser == guestUser){
                        cout<<"Invalid command: upload\n";
                        fs << "Invalid command: upload\n";
                        commanOK = false;
                    }
                    else{
                        DIR *dir = opendir("."); 
                        if(dir){ 

                            struct dirent *ent; 
                            while((ent = readdir(dir)) != NULL) {

                                if(ent->d_name == appendStrings(tokens)){
                                    ok = true; 
                                    commanOK = true;
                                }
                            } 
                        }
                        else { 
                            cout << "Error opening directory" << endl; 
                        }
                        closedir(dir);
                        if(!ok){
                            cout << errors[-4] << "\n";
                            fs << errors[-4] << "\n";
                            commanOK = false;
                        }
                        else{
                            //apendez si userul curent la comanda
                            string bufSend(bufferSend, 0, strlen(bufferSend) - 1);
                            bufSend.append(1u,' ');
                            bufSend.append(currentUser);

                            strcpy(bufferSend,bufSend.c_str());
                        }
                    }
                }
                else if(tokens.at(0) == "share" || tokens.at(0) == "unshare" ||
                        tokens.at(0) == "download" || tokens.at(0) == "delete"){
                    if(currentUser == guestUser){
                        cout << "Invalid command: You must login first\n";
                        fs << "Invalid command: You must login first\n";
                        commanOK = false;
                    }

                    else{
                        //apendez si userul curent la coamnda
                        string bufSend(bufferSend, 0, strlen(bufferSend) - 1);
                        bufSend.append(1u,' ');
                        bufSend.append(currentUser);

                        strcpy(bufferSend,bufSend.c_str());
                    }
                }
                if(commanOK){
                     //trimit mesaj la server
                    n = send(sockfd,bufferSend,strlen(bufferSend), 0);
                    if (n < 0) {
                        char msg[] = "ERROR writing to socket";
                        error(msg);
                    }
                }
                else
                    showPrompt(fs);
            }
            else
                showPrompt(fs);

        }
        if(FD_ISSET(sockfd, &tmp_fds)){
            //primesc de la server
            memset(bufferRecv, 0 , BUFLEN);
            no = recv(sockfd, bufferRecv, sizeof(bufferRecv), 0);
            string stringRecv(bufferRecv);
            if(no > 0){
                if(bufferRecv[0] == 'q'){
                    fs.close();
                    close(sockfd);
                    FD_CLR(sockfd, &read_fds);
                    break;
                }
                else if(tokens.at(0) == "login" && bufferRecv[1] != '3' && bufferRecv[1] != '8'){
                    string user(bufferRecv);
                    currentUser = user;
                }          
                else if(stringRecv != "logout\n"){
                    printf("%s\n",bufferRecv);
                    fs << bufferRecv <<"\n";
                }
                
                if(stringRecv == "closing"){
                    fs.close();
                    close(sockfd);
                    FD_CLR(sockfd, &read_fds);
                    FD_CLR(0, &read_fds);
                    break;
                }
                if(bufferRecv[1] == '8'){
                    exit(0);
                }
            }
            else{
                fs.close();
                close(sockfd);
                FD_CLR(sockfd,&read_fds);
                break;
                cout << "Nothing was received";
            }
            showPrompt(fs);
        }
    }
    return 0;
}