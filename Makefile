CC=g++
LIBSOCKET=-lnsl
CCFLAGS=-Wall -g
SRV=server
SEL_SRV=serverMultiplex
CLT=clientMutiplex

all: $(SEL_SRV) $(CLT)

$(SEL_SRV):$(SEL_SRV).cpp
	$(CC) -std=c++11 -o $(SEL_SRV) $(LIBSOCKET) $(SEL_SRV).cpp

$(CLT):	$(CLT).cpp
	$(CC) -std=c++11 -o $(CLT) $(LIBSOCKET) $(CLT).cpp

clean:
	rm -f *.o *~
	rm -f $(SEL_SRV) $(CLT)


